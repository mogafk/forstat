from flask import Flask, render_template, request
from pymongo import MongoClient

app = Flask(__name__)
client = MongoClient("localhost")
db = client['test_database']

@app.route('/')
def hello_world():
    return render_template("index.html")
@app.route('/newPost', methods=["POST"])
def new_post():
    db.posts.insert({"text": request.form["post"], "header": request.form["header"]})
    return "change to ajax."
@app.route('/feed')
def show_feed():
    print(db.posts)
    return render_template("feed.html", posts=db.posts.find())

if __name__ == '__main__':
    app.run()
